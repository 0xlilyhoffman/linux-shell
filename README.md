# Linux Shell

TTSH demo: https://youtu.be/BTKyUiHVXMc


This project includes a Linux shell program named **TTSH (Tiny Torero Shell)** and an extension of TTSH, **TOSH (Torero Shell)**

## TTSH supports the following:

- Executing commands run in the foreground (e.g. ls -l)
- Executing commands run in the background (e.g. ./siesta &)
- Exiting the shell with the built-in exit command
- Displaying a list of recently executed commands using the built-in history command. history will print out the N most recently entered commands by the user
- Executing commands using the !num syntax where num is the command with a matching command ID from the command history (e.g. !5)
- Changing the current working directory with the build-in cd command


In implementing the shell, I learned to do the following:

- Control processes with fork, execvp, and waitpid system calls
- Write a signal handler
- Execute commands in foreground and background


TTSH operates as a REPL: Read-Evaluate-Print-Loop:

1. Print a prompt and wait for the user to type in a command line
2. Read in the command line entered by the user
3. Parse the command line string into an arg list
4. If its a built in command, the shell program handles the command itself (without forking a child process)
5. If its not a built in command, the shell forks a child process to execute the command

* Executing Foreground Commands: Shell program waits until the child process it forks exits
* Executing Background Commands: Shell program does not wait until the child process it forks to execute before the command exits. The child process will execute the command concurrently with the parent process (the shell, handling other commands). When a child run in the background exits, the shell program reaps it by registering a handler on SIGCHLD. When the parent shell process receives a SIGCHLD, the handler code will run and it calls waitpid to reap the exited child.

## TOSH 
TOSH extends the functionality of TTSH by supporting piping commands and redirection of program input and output. 