/*
 *  The Torero Shell (TOSH)
 *
 *
 *     
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include "history_queue.h"


#include <readline/readline.h>


void parseCMD(char cmdline[], char* argv[], int* bg, int* io_redirection, int* pipe);

void executeForeground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id);
void executeBackground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id);
void executeHistoryEntry(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id, long num);
void executeMostRecentHistory(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id);
void executeCD(char* argv[]);
void handleIORedirection();
void handlePipes();
void findPathToExecutable(char* argv[], char* path_choice[]);
char* testAccess(char* path_choices[]);

void enqueue(char cmdline[], unsigned int* cmd_line_id);
extern HistoryEntry history[MAXHIST];

void childReaper(int signal);


int main (){
    signal(SIGCHLD, childReaper);
    
    int bg; //background
    int io_redirection;
    int pipe; //will be set by parseArguments
    
    char *argv[MAXARGS];    //fixed size = problem
    unsigned int cmd_line_id;
    
    while(1){
        /* (1) read in the next command entered by the user*/
        char *cmdline = readline("tosh$ ");/* NULL indicates EOF was reached, which in this case means someone probably typed in CTRL-d*/
        if (cmdline == NULL) {
            fflush(stdout);
            exit(0);
        }
        fprintf(stdout, "DEBUG: %s\n", cmdline);
        
        /* (2) parse the cmdline  */
        char cmdline_copy[MAXLINE];
        strncpy(cmdline_copy, cmdline, MAXLINE);
        parseCMD(cmdline_copy, argv,&bg, &io_redirection, &pipe);
        
        long num;//for !num command entries
        if(argv[0] != NULL )
            num = strtol(argv[0]+1, NULL, 10);
            
        /* (3) determine how to execute it, and then execute it*/
        /*For blank command*/
        if(argv[0] == NULL )
            continue;
            
        /*For history command*/
        if(strcmp (argv[0], "history") ==0){
            print_queue();
            enqueue(argv[0],&cmd_line_id);
        }
        
        /*For exit command*/
        if(strcmp(argv[0], "exit") == 0){
            printf("May the force be with you\n");
            exit(0);
        }
        
        /*For !num command command*/
        if(cmdline[0] == '!'){
            executeHistoryEntry(argv, bg, cmdline_copy, &cmd_line_id, num);
        }
        
        /*For !! command*/
        if(cmdline[0] == '!' && cmdline[1] == '!'){
            executeMostRecentHistory(argv, bg, cmdline_copy, &cmd_line_id );
        }
        
        /*For cd command*/
        if(strcmp(argv[0], "cd") == 0){
            executeCD(argv);
        }
        
        /*For other command*/
        if((argv[0] != NULL ) &&  (strcmp (argv[0], "history") !=0) && cmdline[0] != '!' && bg==0){
            executeForeground(argv, argv[0], cmdline_copy, &cmd_line_id);
        }
        if(cmdline[0] != '!' && bg>0){
            executeBackground(argv, argv[0], cmdline_copy, &cmd_line_id);
        }
        free(cmdline);
    }
    return 0;
}

/*Signal handler
 *  *Reaps all zombies
 *   */
void childReaper(int signal){
    while( waitpid(0, NULL, WNOHANG)>0){}
}


/*Parse command that was entered
 * Parse into argv format/Constructs argv
 * Determines command name
 * Determines (bg/fg) (I/O redirection?) (pipe?)
 */
void parseCMD(char cmdline[], char* argv[], int* bg, int* io_redirection, int* pipe){
    
    /*Tokenize cmdline into argv array*/
    char* token;
    token = strtok(cmdline, " ");
    int i = 0;
    while(token != NULL){
        /*Check here if need to resize argv?*/
        argv[i] = token;
        token = strtok(NULL, " ");
        i++;
    }
    
    /*Look for special characters &, <, 1>, 2>, | */
    char* b = strchr(cmdline, '&');
    if(b!= NULL) *bg = 1;
    
    char* r = strchr(cmdline, '<');    
    char* r1 = strstr(cmdline, "1>");
    char* r2 = strstr(cmdline, "2>");
    if(r!= NULL || r1 != NULL || r2 != NULL) *io_redirection = 1;
    
    char* p = strchr(cmdline, '|');
    if(p != NULL) *pipe = 1;   
}

 /* Function enqueue adds command to history queue
 * @param cmdline: name of command to enqueue
 *                 entered by user
 * @param cmd_line_id: running count of cmd_line_id number
 *                     passed in by reference and updated per new entry
 */
void enqueue(char cmdline[], unsigned int* cmd_line_id){
        HistoryEntry h;
        h.cmd_num =(*cmd_line_id)++;
        strcpy(h.cmdline, cmdline);
        add_queue(h);
}

/*
 * Executes foreground command entered by user
 * @param char*argv[]: array strings to represent user entered command
 * @param char* arg: placeholder for argv[0]
 * @param cmdline: fully parsed argument- passed to enqueue
 * @param cmd_line_id: running count of cmd_line_id number
 *                      passed in by reference and updated per new entry
 */
void executeForeground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id){
        pid_t pid = fork();
        int status;
        //child
        if(pid == 0){
            int exec_status = execvp(arg, argv);
            if(exec_status == -1)
                printf("ERROR ON EXECVP: Could not run program \"%s\" in current contex\n", arg);
            exit(2);
        }
        //parent
        else if (pid >0){
            waitpid(pid, &status, 0);
        }
        //error
        if(pid == -1){
            printf("ERROR: could not create new process. Exiting... \n");
            exit(1);
        }

        enqueue(cmdline, cmd_line_id);
}

/*
 * Executes foreground command entered by user
 * @param char*argv[]: array strings to represent user entered command
 * @param char* arg: placeholder for argv[0]
 * @param cmdline:fully parsed argument- passed to enqueue
 * @param cmd_line_id: running count of cmd_line_id number
 *                      passed in by reference and updated per new entry
 */
void executeBackground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id){
    pid_t pid = fork();
    //child
    if(pid == 0){
        int exec_status = execvp(arg, argv);
        if(exec_status == -1)
            printf("ERROR ON EXECVP: Could not run program \"%s\" in current contex\n", arg);
        exit(2);
    }

    if(pid == -1){
        printf("ERROR: could not create new process. Exiting... \n");
        exit(1);
    }
    enqueue(cmdline, cmd_line_id);
}

/*
 * Handles !num command
 * @param: argv: array strings to represent user entered command
 * @param: bg holds value returned by parseArguments to indicate if commmand is
 *      background command or foreground command
 * @param cmdline: array of char to represent user entered command
 * @param cmd_line_id running count of cmd_line_id number
 *              passed in by reference and updated per new entry
 * @param num: !num
 */
void executeHistoryEntry(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id, long num){
    int j;
    //search through history queue
    for(j = 0; j < 10; j++){
        //find match of cmd_id
        if( ((history[j]).cmd_num) == num){
            char arg[MAXARGS];
            strcpy(arg, (history[j]).cmdline);
            if (bg == 0)
                executeForeground(argv, arg, arg, cmd_line_id);
            if (bg >0)
                executeBackground(argv, arg, arg,  cmd_line_id);
            /*extraneous case for running "history" from a history entry */
            if((strcmp(((history[j]).cmdline),  "history")) ==0){
                print_queue();
                enqueue("history",cmd_line_id);
            }
            return;
        }
    }
    printf("Invalid command\n");
}

/*
 * Handles !! command - executes most recent command
 * @param: argv: array strings to represent user entered command
 * @param: bg holds value returned by parseArguments to indicate if commmand is
 *      background command or foreground command
 * @param cmdline: array of char to represent user entered command
 * @param cmd_line_id running count of cmd_line_id number
 *              passed in by reference and updated per new entry
 */
void executeMostRecentHistory(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id){
    int j = (*cmd_line_id-1);
    
    char arg[MAXARGS];
    strcpy(arg, (history[j]).cmdline);
    if (bg == 0)
        executeForeground(argv, arg, arg, cmd_line_id);
    if (bg >0)
        executeBackground(argv, arg, arg,  cmd_line_id);

    /*extraneous case for running "history" from a history entry*/                 
    if((strcmp(((history[j]).cmdline),  "history")) ==0){
        print_queue();
        enqueue("history",cmd_line_id);
    }
    
}

void executeCD(char* argv[]){
    int ret;
    
    if(argv[1] == NULL) {
         ret = chdir("HOME");
    }
   
    if(strcmp(argv[1],"..") ==0) {
         ret = chdir("OLDPWD");
    }
    
    else{
        ret = chdir(argv[1]);
    } 
}


/*Must create
 *     int MAXPATH = 10;
 *     char* path_choices[MAXPATH];
 *     right before calling this function
 *    
 */ 
void findPathToExecutable(char* argv[], char* path_choices[]){
    char* first_word = argv[0];

    if((first_word[0] == '/') || (first_word[0] == '.' && first_word[1] == '/') || (first_word[0] == '.' && first_word[1] == '.' && first_word[2] == '/') )
        return;
    else{
        /*Find Path*/
        char* path = getenv("PATH");
        
        /*Store possible paths in array of stirngs - tokenize path*/
        char* token;
        token = strtok(path, ":");
        int i = 0;
        while(token != NULL){
            path_choices[i] = token;
            token = strtok(NULL, ":");
            i++;
        }   
    }
}



char* testAccess(char* path_choices[]){
    int MAXPATH = 10;
    int ret;
    char* path_name = NULL;
    int i;
    for(i = 0; i < MAXPATH; i++){
        ret = access(path_choices[i], X_OK);
        if(ret == 0){
            path_name=path_choices[i];
            break;
        }        
    }
    
    return path_name;
}
