/*
 * The Tiny Torero Shell (TTSH)
 *
 * Small shell program
 * Supports running built in bash commands
 * as well as the following user defined commands:
 * 	History: prints out most recent 10 items
 * 	!num: runs item "num" in history queue
 *	exit prints friendly farewell and exits shell
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include "parse_args.h"
#include "history_queue.h"

void executeForeground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id);
void executeBackground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id);
void executeHistoryEntry(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id, long num);
void enqueue(char cmdline[], unsigned int* cmd_line_id);
void childReaper(int signal);
extern HistoryEntry history[MAXHIST];

int main( ){
	char cmdline[MAXLINE];
	char *argv[MAXARGS];
	int bg;
	unsigned int cmd_line_id;
	
	/*User defined signal handler*/
	signal(SIGCHLD, childReaper);
	
	while(1){
		// (1) print the shell prompt
		fprintf(stdout, "ttsh> ");
		fflush(stdout);
		
		// (2) read in the next command entered by the user
		if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin)) {
			perror("fgets error");
		}
		
		/* End of file (ctrl-d) */
		if (feof(stdin)){
			fflush(stdout);
			exit(0);
		}
		
		// (3) make a call to parseArguments function to parse it into its argv format
		char cmdline_copy[MAXLINE];
		strncpy(cmdline_copy, cmdline, MAXLINE);
		bg = parseArguments(cmdline_copy, argv);
		
		long num;//for !num command entries
		if(argv[0] != NULL )num = strtol(argv[0]+1, NULL, 10);
		
		// (4) determine how to execute it, and then execute it
		/*For blank command*/
		if(argv[0] == NULL )
			continue;
		/*For history command*/
		if(strcmp (argv[0], "history") ==0){
			print_queue();
			enqueue(argv[0],&cmd_line_id);
		}
		
		/*For exit command*/
		if(strcmp(argv[0], "exit") == 0){
			printf("May the force be with you\n");
			exit(0);
		}
		
		/*For !num command command*/
		if(cmdline[0] == '!'){
			executeHistoryEntry(argv, bg, cmdline_copy, &cmd_line_id, num);
		}
		
		/*For other command*/
		if((argv[0] != NULL ) &&  (strcmp (argv[0], "history") !=0) && cmdline[0] != '!' && bg==0){
			executeForeground(argv, argv[0], cmdline_copy, &cmd_line_id);
		}
		if(cmdline[0] != '!' && bg>0){
			executeBackground(argv, argv[0], cmdline_copy, &cmd_line_id);
		}
	}
	return 0;
}
/*
 * Function enqueue adds command to history queue
 * @param cmdline: name of command to enqueue
 * 		   entered by user
 * @param cmd_line_id: running count of cmd_line_id number
 * 		       passed in by reference and updated per new entry
 */
void enqueue(char cmdline[], unsigned int* cmd_line_id){
	HistoryEntry h;
	h.cmd_num =(*cmd_line_id)++;
	strcpy(h.cmdline, cmdline);
	add_queue(h);
}

/*
 * Executes foreground command entered by user
 * @param char*argv[]: array strings to represent user entered command
 * @param char* arg: placeholder for argv[0]
 * @param cmdline: fully parsed argument- passed to enqueue
 * @param cmd_line_id: running count of cmd_line_id number
 * 			passed in by reference and updated per new entry
 */
void executeForeground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id){
	pid_t pid = fork();
	int status;
	if(pid == 0){//child
		int exec_status = execvp(arg, argv);
		if(exec_status == -1)
			printf("ERROR ON EXECVP: Could not run program \"%s\" in current contex\n", arg);
		exit(2);
	}else if (pid >0){//parent
		waitpid(pid, &status, 0);
	}
	if(pid == -1){ //error
		printf("ERROR: could not create new process. Exiting... \n");
		exit(1);
	}
	enqueue(cmdline, cmd_line_id);	
}


/*
 * Executes foreground command entered by user
 * @param char*argv[]: array strings to represent user entered command
 * @param char* arg: placeholder for argv[0]
 * @param cmdline:fully parsed argument- passed to enqueue
 * @param cmd_line_id: running count of cmd_line_id number
 *                      passed in by reference and updated per new entry
 */
void executeBackground(char* argv[], char* arg, char cmdline[], unsigned int* cmd_line_id){
	pid_t pid = fork();
	//child
	if(pid == 0){
		int exec_status = execvp(arg, argv);
		if(exec_status == -1)
			printf("ERROR ON EXECVP: Could not run program \"%s\" in current contex\n", arg);
		exit(2);
	}
	//error
	if(pid == -1){
		printf("ERROR: could not create new process. Exiting... \n");
		exit(1);
	}
	enqueue(cmdline, cmd_line_id);
}

/*
 * Handles !num command
 * @param: argv: array strings to represent user entered command
 * @param: bg holds value returned by parseArguments to indicate if commmand is
 * 	background command or foreground command
 * @param cmdline: array of char to represent user entered command
 * @param cmd_line_id running count of cmd_line_id number
 * 		passed in by reference and updated per new entry
 * @param num: !num
 */
void executeHistoryEntry(char* argv[], int bg, char cmdline[],unsigned int* cmd_line_id, long num){
	int j;
	//search through history queue
	for(j = 0; j < 10; j++){
		//find match of cmd_id
		if( ((history[j]).cmd_num) == num){
			char arg[MAXARGS];
			strcpy(arg, (history[j]).cmdline);
			if (bg == 0)
				executeForeground(argv, arg, arg, cmd_line_id);
			if (bg > 0)
				executeBackground(argv, arg, arg,  cmd_line_id);
			//extraneous case for running "history" from a history entry
			if((strcmp(((history[j]).cmdline),  "history")) ==0){
				print_queue(); 
				enqueue("history",cmd_line_id);
			}
			return;
		}
	}
	printf("Invalid command\n"); 
}

/*Signal handler
 * reaps all zombies
 */
void childReaper(int signal){
	while( waitpid(0, NULL, WNOHANG)>0){}
}


