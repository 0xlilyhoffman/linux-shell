/*
 * The Tiny Torero Shell (TTSH)
 *
 * Add your top-level comments here.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "history_queue.h"

// global variables: add only globals for history list state
//                   all other variables should be allocated on the stack
// static: means these variables are only in scope in this .c module
HistoryEntry history[MAXHIST]; 
static int queue_start = 0; 	// the index where queue begins
static int queue_next = 0;      // the index where we will insert next item
static int  queue_size = 0; 	// the number of items in the queue
                                // Note that the capacity (i.e. max size) is
                                      

void add_queue(HistoryEntry val){
    history[queue_next] = val;

    queue_size++;

    if(queue_size == 1){
        queue_start = 0;
        queue_next = 1;
    }


    if( (queue_size != 1) && (queue_size < MAXHIST)){
        queue_start = queue_next;
        queue_next++;
    }
    if(queue_size >=  MAXHIST){
        queue_start =  queue_next;
        queue_next = (queue_size)%MAXHIST;
    }
}


void print_queue(){
    int i;
    if(queue_size <= MAXHIST){
        for(i = 0; i< queue_size; i++){
            printf("%d %s\n", history[i].cmd_num, history[i].cmdline);
        }
    }

    else if(queue_size > MAXHIST){
        for(i = queue_start+1; i < MAXHIST; i++){
            printf("%d %s\n", history[i].cmd_num, history[i].cmdline);
        }

        for(i = 0; i <= queue_start; i++){
            printf("%d %s\n", history[i].cmd_num, history[i].cmdline);
        }
    }
    printf("\n");
}
